using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopGunController : Singleton<ShopGunController>
{
    public BtnGun buttonGun;
    public GameObject gridLayout;
    public Gun[] listGun;
    public BtnGun cur_Gun;
    public BtnGun cur_GunSelected;
    public Scrollbar scrollbar;
    public ToggleGroup toggleGroup;
    // Start is called before the first frame update
    void Start()
    {
        init();
        
    }

    //initialize BtnGun list and add to group toggle
    public void init()
    {
        for (int i = 0; i < listGun.Length; i++)
        {
            BtnGun g = Instantiate(buttonGun, gridLayout.transform);
            g.UpdateBtnGun(listGun[i]);
            g.toggle.group = toggleGroup;
            if (i == 0)
            {
                g.text.text = "Used";
                cur_Gun = g;
                cur_GunSelected = g;
                g.isRentOut = true;
            }  
        }
        cur_Gun.toggle.isOn = true;
        PrimaryWeaPonController.Ins.ImageGun.sprite = cur_Gun.icon.sprite;
        PlasmaGunController.Ins.ShowInfoGun(cur_Gun.gun);
        scrollbar.size = 0.13f;
        scrollbar.value = 0.001f;
    }

    //Update text for 2 buttons that need to change text
    public void UpdateListBtnGun(BtnGun cur_Gun, BtnGun select_Gun)
    {
        cur_Gun.UpdateBtnGun(cur_Gun.gun);
        select_Gun.UpdateBtnGun(select_Gun.gun);
    }
}
