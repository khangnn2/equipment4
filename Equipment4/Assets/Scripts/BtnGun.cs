using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BtnGun : MonoBehaviour
{
    public Image icon;
    public Text text;
    public Gun gun;
    public bool isRentOut = false;
    public Toggle toggle;

    //Change effects for selected gun
    public void Selected()
    {
        if(toggle.isOn)
        {
            ShopGunController.Ins.cur_GunSelected = this;
            PlasmaGunController.Ins.ShowInfoGun(gun);
        }
    }

    //Update text for BtnGun
    public void UpdateBtnGun(Gun g)
    {
        gun = g;
        icon.sprite = g.icon;

        if(isRentOut)
        {
            if(this==ShopGunController.Ins.cur_Gun)
            {
                text.text = "Used";
                text.color = new Color(0.16f, 0.75f, 0.1f);

            }
            else
            {
                text.text = "Rended Out";
                text.color= new Color(0.9f, 0.65f, 0.27f);
            }
        }
        else
            text.text = "";
    }
}
