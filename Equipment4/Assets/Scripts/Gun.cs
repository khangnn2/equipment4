using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Gun
{
    public int idGun;
    public float damage;
    public float dispersion;
    public float rateOfFire;
    public float reloadSpeed;
    public float ammunition;
    public Sprite icon;
    
}
