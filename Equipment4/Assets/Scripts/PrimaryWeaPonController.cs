using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrimaryWeaPonController : Singleton<PrimaryWeaPonController>
{
    public GameObject pnlNotBought;
    public GameObject pnlUse_RentOut;
    public Image ImageGun;
    private bool isUse;
    public Text txtUse_RentOut;

    private void Start()
    {
        Resume();
    }

    //Event of Button Use
    public void Use()
    {
        if(ShopGunController.Ins.cur_GunSelected!= ShopGunController.Ins.cur_Gun)
        {
            pnlUse_RentOut.SetActive(true);
            isUse = true;
            txtUse_RentOut.text = "Do you want to using this gun?";
        }
        
    }

    //Event of Button Accept Use, Accept Rent out
    public void Accept()
    {
        if(isUse)
        {
            if (ShopGunController.Ins.cur_GunSelected.isRentOut)
            {
                BtnGun g = ShopGunController.Ins.cur_Gun;
                ShopGunController.Ins.cur_Gun = ShopGunController.Ins.cur_GunSelected;

                //Update text for previous button, current button
                ShopGunController.Ins.UpdateListBtnGun(g, ShopGunController.Ins.cur_Gun);
            }
            else
                pnlNotBought.SetActive(true);
            
        }
        else
        {
            
            ShopGunController.Ins.cur_GunSelected.isRentOut = true;

            //Update text for current button, selected button
            ShopGunController.Ins.UpdateListBtnGun(ShopGunController.Ins.cur_Gun, ShopGunController.Ins.cur_GunSelected);
        }
        pnlUse_RentOut.SetActive(false);

    }
    //Event of Button Rent Out
    public void RentOut()
    {
        isUse = false;
        if(ShopGunController.Ins.cur_GunSelected.isRentOut==false)
        {
            pnlUse_RentOut.SetActive(true);
            txtUse_RentOut.text = "Do you want to Rent out?";
        }
            
    }

    //Event for button OK, button No, Hide unused panels
    public void Resume()
    {

        pnlNotBought.SetActive(false);
        pnlUse_RentOut.SetActive(false);

    }
}
